# RESTful API dengan express sequelize ejs

Tabel database

![erd](./public/images/erd.png)


## Install dependency

```bash
# Pengguna NPM
npm install

```

## Config env variable
config env

## Run sequelize

```
sequelize db:create (jika belum ada database)
sequelize db:migrate
sequelize db:seed:all
```

## Jalankan server

```
npm run dev
```
