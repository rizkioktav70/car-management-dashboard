const { cars } = require('../models')
const imagekit = require('../lib/imageKit')

const createCars = async(req, res) => {
    try {
        const { name, price, type, size } = req.body;
        // untuk dapat extension file nya
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, //required
            fileName: `${req.file.originalname}.${ext}`, //required
        })
        console.log(img.url)

        const newCars = await cars.create({
            name,
            type,
            price,
            size,
            image: img.url
        })
      res.status(201).json({
      status: "success",
      data: { newCars },
    });
    } catch (error) {
        res.status(400).json({
            status: "fail",
            message: error.message
        })

    }
}

const createDataCar = async (req, res) => {
    const { name, type, price, size } = req.body
    try {
        const newCar = await cars.create({
            name,
            type,
            price,
            size
        })

        res.status(201).json({
            status: 'success',
            data: {
                newCar
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findDataCar = async (req, res) => {
    try {
        const getCar = await cars.findAll()
        res.status(200).json({
            status: 'Success',
            data: {
                getCar
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findDataCarById = async (req, res) => {
    try {
        const getCar = await cars.findOne({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                getCar
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const updateDataCar = async (req, res) => {
    try {
        const { name, type, price, size } = req.body
        const id = req.params.id
        await cars.update({
            name,
            type,
            price,
            size
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id,
                name,
                type,
                price,
                size
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteDataCar = async (req, res) => {
    try {
        const id = req.params.id
        await cars.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            status: 'success',
            message: `car dengan id ${id} terhapus`
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createDataCar,
    findDataCar,
    findDataCarById,
    updateDataCar,
    deleteDataCar,
    createCars
}